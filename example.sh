#!/bin/sh
MYCUSTOMTAB='    '
ONETAB=' '
FORTAB='   '
let nDevices=1
version=1.0
echo    "${ONETAB}${MYCUSTOMTAB}${MYCUSTOMTAB}${MYCUSTOMTAB}____"
echo    "${FORTAB}${MYCUSTOMTAB}${MYCUSTOMTAB}_(____)_"
echo    "${MYCUSTOMTAB}___ooO_(_o__o_)_Ooo___"
echo -e	"${MYCUSTOMTAB}  EmulatorController \n"

echo ' '


coldBootNow(){
	read -p ':: Elija el dispositivo que desea arrancar en frio > ' uservar
	echo 	"Procesando informacion......."
	echo 	${DevicesArray[$uservar]}
	echo 	$($EMULATOR "@"${DevicesArray[$uservar]} -no-snapshot-load)
}

runDevice(){
	read -p ':: Elija el dispositivo que desea utilizar > ' uservar
	echo 	"Iniciando........"
	echo 	${DevicesArray[$uservar]}
	echo 	$($EMULATOR -avd ${DevicesArray[$uservar]})

}


findDevices(){
	Devices=$($EMULATOR -list-avds)
	for value in $Devices
	do
		echo $nDevices") "$value
		DevicesArray[$nDevices]=$value
		nDevices=$(($nDevices+1))
	done 
	echo 	''


}

listDevices(){	
	echo 	"LISTA DE DISPOSITIVOS VIRTUALES"
	findDevices
}

showHelp(){
	echo ''
	echo 'Usage: emulator [--help|--version] <command>'

echo 'Global options:'
echo '  --help ...... show this help and exit'
echo '  --version ... show version information and exit'

echo 'Commands:'
echo '  -l '
echo '	List all available virtual devices'

echo '  -r'
echo '	run the selected virtual device'

echo '  -c'
echo '	Cold boot now'
}

case $1 in
	"-l")
		#echo "quiere la opcion 1"
		listDevices
		;;
	"-r")
		#echo "quiere la opcion 2"
		echo "DiSPOSITIVOS VIRTUALES DISPONIBLES"
		findDevices
		runDevice
		;;
	"-c")
		#echo "quiere la opcion 3"
		echo "Cold Boot Now"
		findDevices
		coldBootNow
		;;
	"--version")
		echo "Version "$version
		;;
	"--help")
		echo "Ayuda"
		showHelp
		;;
	*)
		echo "Comando incorrecto"
		;;
	esac




